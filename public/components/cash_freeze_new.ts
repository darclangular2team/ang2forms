import {Component, ElementRef, OnInit} from 'angular2/core';
import {MATERIAL_DIRECTIVES, MdDialog, Media, MdDialogConfig, MdDialogBasic, MdDialogRef} from "ng2-material/all";
import {FORM_DIRECTIVES} from "angular2/common";
import {FORM_PROVIDERS, CORE_DIRECTIVES} from "angular2/common";
import {Router} from "angular2/router";
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';

import * as CashFreeze from "../models/cashFreeze";
import {CashFreezeService} from "../http_services/cashFreezeService";

@Component({
    selector: 'cash-freeze-new',
    templateUrl: 'templates/cash_freeze_New.html',
    styleUrls: ['stylesheets/style.css'],
    directives: [MATERIAL_DIRECTIVES, FORM_DIRECTIVES],
    providers: [FORM_PROVIDERS]
})

export class cashFreezeNew {
    cashFreezeStrObservable:Observable<CashFreeze.CashStore>;
    cashFreeze:CashFreeze.CashFreeze;

    constructor(public router:Router,
                private cashDataService:CashFreezeService,
                private store:Store<CashFreeze.CashStore>) {

        this.cashFreezeStrObservable = store.select('cashFreeze');
        this.cashFreezeStrObservable.subscribe(()=> {
            console.log('in store...');
            if (this.store.getValue().cashFreeze != null) {
                this.cashFreeze = this.store.getValue().cashFreeze;
                console.log('in freeze...');
                console.log(this.cashFreeze)
            } else {
                console.log('in create new Object...');
                this.cashFreeze = new CashFreeze.CashFreeze();
                let payload = this.cashFreeze;
                this.store.dispatch({type: 'GET_CASH_FREEZE_DETAILS', payload});

                this.cashDataService.getBranchSummary();
            }
        });
    }

    onListClick(data) {
        this.store.getValue().cashFreeze.selectedSummary = data;
        this.cashDataService.getCashFreezeDetails()
            .map(payload => ({type: 'GET_BRANCH_FREEZE_DETAIL', payload}))
            .subscribe(action => this.afterCashFreezeDetailService(action));
    }
    private afterCashFreezeDetailService(action:any) {
        this.store.dispatch(action);
        this.router.navigate(['CashDetailNew']);
    }
}
