import {Component} from 'angular2/core';
import {MATERIAL_DIRECTIVES} from "ng2-material/all";
import {FORM_DIRECTIVES} from "angular2/common";
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import {CashStore} from "../models/cashFreeze";

@Component({
    selector: 'cash-detail-new',
    templateUrl: 'templates/cashFreezeDetailsNew.html',
    directives: [MATERIAL_DIRECTIVES, FORM_DIRECTIVES]
})

export class CashFreezeDetailsNewTable {
    cashFreezeStrObservable:Observable<CashStore>;

    constructor(private store:Store<CashStore>) {
        this.cashFreezeStrObservable = store.select('cashFreezeTable');
        this.cashFreezeStrObservable.subscribe(()=> {
            console.log('in CashFreezeDetailsNewTable.....');
        });
    }

}

