import {bootstrap}  from 'angular2/platform/browser';
import {MATERIAL_BROWSER_PROVIDERS} from "ng2-material/all";
import {provideStore} from '@ngrx/store';
import {HTTP_PROVIDERS} from 'angular2/http';
import {ROUTER_PROVIDERS, LocationStrategy, HashLocationStrategy } from 'angular2/router'
import {provide}    from 'angular2/core'
import {FORM_PROVIDERS,CORE_DIRECTIVES} from "angular2/common";

/********Imports Components ************************/
import {cashFreeze} from '../reducers/cash_freeze_reducer'
import {RouteComponent} from './route.component';
import {CashFreezeService} from "../http_services/cashFreezeService";
import {WindowService} from '../auth/window.service';
import {AuthService} from '../auth/auth.service';
import {CookieService} from '../auth/cookies.service';
import {loginInfo} from '../reducers/login_reducer'
/**************************************************/

bootstrap(RouteComponent, [CookieService, AuthService, WindowService, CashFreezeService, HTTP_PROVIDERS, MATERIAL_BROWSER_PROVIDERS,FORM_PROVIDERS,
    ROUTER_PROVIDERS, provide(LocationStrategy, {useClass: HashLocationStrategy }), provideStore({cashFreeze, loginInfo})]);
