import {Component, Input, OnDestroy, ApplicationRef} from "angular2/core";
import {Media, MATERIAL_DIRECTIVES, SidenavService} from "ng2-material/all";
import {ROUTER_DIRECTIVES, RouteConfig, Router} from "angular2/router";
import {Observable} from 'rxjs/Observable';
import {Store} from '@ngrx/store';

//Import custom components
import  {cashFreezeNew} from './cash_freeze_new';
import {AuthService, LoginInfo, LoginStore} from '../auth/auth.service';
import {CookieService} from '../auth/cookies.service';
import {CashFreezeDetailsNewTable} from "./cashFreezeDetailsNew";

//Configure your routes
@RouteConfig([
    {path: '/csh', as: 'CashFreeze', component: cashFreezeNew},
    {path: '/cshdtlnw', as: 'CashDetailNew', component: CashFreezeDetailsNewTable}
])

@Component({
    selector: 'route-component',
    //styleUrls: ['app/ui/app.css'],
    templateUrl: 'templates/main.html',
    directives: [MATERIAL_DIRECTIVES, ROUTER_DIRECTIVES],
    host: {
        '[class.push-menu]': 'fullPage'
    }
})

export class RouteComponent implements OnDestroy{
    static SIDE_MENU_BREAKPOINT: string = 'gt-md';
    @Input()
    fullPage: boolean = this.media.hasMedia(RouteComponent.SIDE_MENU_BREAKPOINT);

    formTitle = 'Dashboard';
    private loginInfo: LoginInfo;
    userName:String = '';
    authenticated:boolean = false;
    authServiceObservable:Observable<LoginInfo>;
    private _subscription = null;
    constructor(
        public media: Media,
        public appRef: ApplicationRef,
        public router: Router,
        private _sidenav: SidenavService,
        private authService:AuthService,
        private  cookies:CookieService,
        private store:Store<LoginStore>) {

        let query = Media.getQuery(RouteComponent.SIDE_MENU_BREAKPOINT);
        this._subscription = media.listen(query).onMatched.subscribe((mql: MediaQueryList) => {
            this.fullPage = mql.matches;
            this.appRef.tick();
        });


        this.authServiceObservable = this.store.select("loginInfo");
        this.authServiceObservable.subscribe(()=> {
            this.loginInfo = this.store.getValue().loginInfo;
            if(this.loginInfo && this.loginInfo.authenticated){
                this.navigate(['CashFreeze']);
                this.userName = this.loginInfo.mailId;
                this.authenticated = this.loginInfo.authenticated;
            }
        });
    }

    onLogin(){
        if(this.loginInfo && this.loginInfo.authenticated)
            this.authService.doLogout()
        else
            this.authService.doLogin();
    }

    get idCookie() {
        return this.cookies.getCookie('id');
    }

    ngOnDestroy(): any {
        this._subscription.unsubscribe();
    }

    showMenu(event?) {
        this._sidenav.show('menu');
    }

    navigate(to: any) {
        this.formTitle = to;
        this.router.navigate(to);
    }
}