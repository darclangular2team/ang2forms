import {Component} from 'angular2/core';
import {MATERIAL_DIRECTIVES} from "ng2-material/all";

@Component({
    selector: 'spinner',
    styleUrls: ['stylesheets/spinner.css'],
    //I'm using in modal-backdrop classes from bootstrap
    template:
        `<div class="in modal-backdrop spinner-overlay"></div>
     <div class="spinner-message-container" aria-live="assertive" aria-atomic="true">
        <div class="spinner-message" [ngClass]="spinnerMessageClass">
        <div layout="row" layout-sm="column" layout-align="space-around">
    <md-progress-circular [diameter]="70" mode="indeterminate"></md-progress-circular>
  </div>
  </div>
    </div>`,
    directives: [MATERIAL_DIRECTIVES],
})
export class SpinnerComponent {
    state = {
        message: 'Please wait...'
    };
}