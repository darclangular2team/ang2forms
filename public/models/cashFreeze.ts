export const GET_BRANCH_SUMMARY: string = 'GET_BRANCH_SUMMARY';
export const GET_BRANCH_FREEZE_DETAIL: string = 'GET_BRANCH_FREEZE_DETAIL';

export class CashNotesDetail{
    note1000: number
    note500: number
    note100: number
    note50: number
    note20: number
    note10: number
    note5: number
    coinsValue: number
    remarks: String
    selected:boolean
    freezedDate: String
    amount: String
}

export class CashFreeze {
    selectedGlAccount: String
    selectedBranchId: String
    lastFreezedDate: String
    openingBalance: String
    denoMaintain: String
    branchDetails: any
    glAccountDetail: any
    cashNoteDetail: CashNotesDetail
    cashFreezeTable: CashNotesDetail[]
    selectedSummary: any;
}

export interface CashStore {
    cashFreeze: CashFreeze
    cashNotesDetail :CashNotesDetail[];
}



