import * as CashFreeze from '../models/cashFreeze';

export const cashFreeze = (state:any = null, {type, payload}) => {
    switch (type) {
        case CashFreeze.GET_BRANCH_SUMMARY:
            return payload;

        case CashFreeze.GET_BRANCH_FREEZE_DETAIL:
            state.cashFreezeTable = payload.freezeDetail;
            return state;

        default:
            return state;

    }
};
