export const loginInfo = (state: any = null, {type, payload}) => {
    switch (type) {
        case 'LOGIN_INFO':
            return payload;

        default:
            return state;

    }
};
