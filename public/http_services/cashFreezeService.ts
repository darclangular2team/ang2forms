import {Http, Response} from 'angular2/http';
import {Store} from '@ngrx/store';
import {Injectable} from 'angular2/core';

import * as CashFreeze from "../models/cashFreeze";
import {LoginStore} from "../auth/auth.service";

@Injectable()
export class CashFreezeService {
    constructor(private http:Http, private store:Store<CashFreeze.CashStore>,
                private loginStore:Store<LoginStore>) {
    }

    public getBranchSummary() {
        this.http.get('/branchCashSummery?mailId=' + this.loginStore.getValue().loginInfo.mailId)
            .map(res => res.json())
            .map(payload => ({type: 'GET_BRANCH_SUMMARY', payload}))
            .subscribe(action => this.store.dispatch(action));
    }

    public getCashFreezeDetails() {
        console.log('getCashFreezeDetails....');
        return this.http.get('/cashDetails?branchId=' + this.store.getValue().cashFreeze.selectedSummary.branchId + '&freezeDate=' +
                this.store.getValue().cashFreeze.selectedSummary.freezeDate)
            .map(res => res.json());

    }
}
