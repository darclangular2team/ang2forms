var express = require('express');
var router = express.Router();

//Don't forget this line. Otherwise it shows error
var path = require('path');

/* GET home page. If you give /dashboard then you should given http://localhost:3000/dashboard otherwise it shows error*/
router.get('/', function(req, res, next) {
  res.sendFile(path.join(__dirname, '../', 'views', 'index.html'));
});

module.exports = router;
