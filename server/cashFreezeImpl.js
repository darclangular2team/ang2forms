var express = require('express');
var path = require('path');
var router = express.Router();
var http = require("http");

var mongodb = require('mongodb');

var MongoClient = mongodb.MongoClient;

var url = 'mongodb://192.168.100.120:27017/CashFreezeDB';

router.get('/cashDetails', function (request, response) {
    var branchId = request.query.branchId;
    var freezeDate = request.query.freezeDate;
    MongoClient.connect(url, function (err, db) {
        var mainObject = {'error': false, 'msg': '', 'freezeDetail': []};

        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
            mainObject.error = true;
            mainObject.msg = err.message;
            response.end(JSON.stringify(mainObject));
        } else {
            var freezeDetails = db.collection('FreezeDetails');
            var cursor = freezeDetails.find({
                $and: [{closingDate: {$gt: freezeDate}}, {branchId: {$eq: branchId}}
                ]
            });

            var freezeDetail = [];
            cursor.each(function (err, item) {
                if (item == null) {
                    cursor.toArray(function (err, items) {
                        db.close();
                    });
                    console.log(freezeDetail);
                    mainObject.freezeDetail = freezeDetail;
                    response.end(JSON.stringify(mainObject));
                } else
                    freezeDetail.push(item);
            });
        }
    });
});

router.get('/branchCashSummery', function (request, response) {
    var mailId = request.query.mailId;
    MongoClient.connect(url, function (err, db) {
        var mainObject = {'error': false, 'msg': '', 'summary': []};
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
            mainObject.error = true;
            mainObject.msg = err.message;
            response.end(JSON.stringify(mainObject));
        } else {
            if (err) {
                console.log('Unable to reterive data due to Error:', err);
                mainObject.err = true;
                mainObject.msg = err.message;
                response.end(JSON.stringify(mainObject));
            }
            else {
                db.eval("FreezeSummary('" + mailId + "')", function (error, result) {
                    //console.log(result._batch);
                    db.close();
                    mainObject.summary = result._batch;
                    response.end(JSON.stringify(mainObject));
                });
            }
        }
    });
});

module.exports = router;